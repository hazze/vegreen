# Client Dev #
```
cd client
npm install
npm run serve
```

# Client Build #
```
cd client
npm install
npm run build
```


# Server Dev #
```
cd server
npm install
npm run dev
```

# Server Build #
```
cd server
npm install
npm run build
```