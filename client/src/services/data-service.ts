class DataService {
    baseUrl = 'http://localhost:3000/api/';
    
    private static instance: DataService
    public static get Instance() {
        return this.instance || (this.instance = new this())
    }

    private headers = {
        'Content-Type': 'application/json'
    }

    async getAllVegetables(): Promise<any> {
        return await fetch(this.baseUrl + 'veggies/')
            .then(res => res.text());
    }

    async getVegetable(id: number): Promise<any> {
        return await fetch(this.baseUrl + 'veggies/' + id)
            .then(res => res.text());
    }

    async deleteVegetable(id: number): Promise<any> {
        return await fetch(this.baseUrl + 'veggies/' + id, {
                method: 'DELETE' 
            })
            .then(res => res.text());
    }

    async addVegetable(item: any): Promise<any> {
        return await fetch(this.baseUrl + 'veggies/', {
                headers: this.headers,
                method: 'POST',
                body: JSON.stringify(item) 
            })
            .then(res => res.text());
    }

    async updateVegetable(item: any): Promise<any> {
        return await fetch(this.baseUrl + 'veggies/', {
                headers: this.headers,
                method: 'PATCH',
                body: JSON.stringify(item) 
            })
            .then(res => res.text());
    }
}
export const dataService = DataService.Instance;
