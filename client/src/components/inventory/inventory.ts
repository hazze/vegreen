import { Vegetable } from '@/models/vegetable';
import { dataService } from '@/services/data-service';
import { Component, Vue } from 'vue-property-decorator';
import { MessageBox } from 'element-ui';

import VegetableComponent from '@/components/vegetable/vegetable.vue';

@Component({
    components: {
        VegetableComponent
    }
})
export default class InventoryComponent extends Vue {
    inventory: Vegetable[] = [];
    vegetable: Vegetable | null = null;

    mounted(): void {
        this.reload();
    }

    reload(): void {
        dataService.getAllVegetables().then(res => {
            this.inventory = JSON.parse(res);
        });
    }

    reset(): void {
        this.vegetable = null;
        this.$nextTick(() => {
			(this.$refs.inventoryTable as any).setCurrentRow();
		});
    }

    getVegetable(id: number, col: any): void {
        if (col.property === 'delete') return;
        dataService.getVegetable(id).then(res => this.vegetable = JSON.parse(res));
    }

    handleDelete(id: number): void {
        MessageBox.confirm('Are you sure you want to delete this item from the inventory?', 'Warning', {
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel',
            type: 'warning'
        }).then(() => {
            dataService.deleteVegetable(id).then(res => {
                if (res === 'OK') {
                    this.reload();
                }
            });
            this.vegetable = null;
        }).catch(() => {});
    }
}
