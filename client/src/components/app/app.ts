import { Component, Vue } from 'vue-property-decorator';
import InventoryComponent from '@/components/inventory/inventory.vue';

@Component({
  components: {
    InventoryComponent
  },
})
export default class App extends Vue {}