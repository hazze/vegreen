import { Component, Prop, Vue, Watch } from 'vue-property-decorator';
import { Vegetable } from '@/models/vegetable';
import { dataService } from '@/services/data-service';

@Component
export default class VegetableComponent extends Vue {
    @Prop()
    vegetable!: Vegetable | null;
    newVegetable: Vegetable = { id: 0, name: '', price: 0, amount: 0};

    @Watch("vegetable")
    vegetableWatcher() {
        if (this.vegetable) {
            this.newVegetable = this.vegetable;
        }
    }

    async submit(): Promise<void> {
        let result;
        if (this.newVegetable.id)
            result = await dataService.updateVegetable(this.newVegetable).then(res => res);
        else 
            result = await dataService.addVegetable(this.newVegetable).then(res => res);
        
        if (result === 'OK') {
            this.$emit('reload');
            this.reset();
        }
    }

    reset(): void {
        this.$emit('reset');
        this.newVegetable = { id: 0, name: '', price: 0, amount: 0};
    }

    getButtonInfo(type: string): string {
        if (type === 'icon')
            return this.newVegetable.id === 0 ? 'el-icon-plus' : 'el-icon-check';
        else if (type === 'text')
            return this.newVegetable.id === 0 ? 'Add' : 'Save';
        return '';
    }
}
