import Vue from 'vue'
import app from './components/app/app.vue'
import './plugins/element.js'

Vue.config.productionTip = false

new Vue({
  render: h => h(app),
}).$mount('#app')
