import { JsonDB } from 'node-json-db';
import { Config } from 'node-json-db/dist/lib/JsonDBConfig'
import { Vegetable } from '../models/vegetable';

export default class DatabaseHandler {
    private db = new JsonDB(new Config('db', true, true, '/'));;

    constructor() {
        console.log("created db");
    }

    private getNextId(name: string): number {
        try {
            const data = this.db.getData('/' + name);
            const nextId = Math.max(...data.map((x: any) => x.id));
            return nextId + 1;
        }
        catch (error) {
            return 1;
        }
        
    }

    public getAll(): any {
        return this.db.getData('/');
    }

    public getById(id: number, type: string): any {
        const data = this.db.getData('/' + type);
        return data.find((x: any) => x.id === id);
    }

    public add(item: Vegetable): void {
        try {
            this.db.push('/', { id: this.getNextId(item.name), name: item.name, price: item.price }, false);
        }
        catch (error) {
            console.log('error', error);
        }
    }
}
