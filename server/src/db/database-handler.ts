import mssql, { config, MAX } from 'mssql';
import { Vegetable } from '../models/vegetable';

export default class DatabaseHandler {
    public db!: mssql.ConnectionPool;
    private config: config = {
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        server: process.env.DB_HOST!,
        database: process.env.DB_DATABASE,

    }
    
    constructor() {}

    public init() {
        this.db = new mssql.ConnectionPool(this.config, error => {
            if (error) {
                console.log('Connection failed to database.', error);
            } else {
                console.log('Connection to database established.');
            }
        });
    }

    async getAll(): Promise<Vegetable[]> {
        const query = 'SELECT * FROM Vegetables';
        const request = new mssql.Request(this.db);
        const result = await request.query(query);
        const data: Vegetable[] = [...result.recordset];
        return data;
    }

    async getById(id: number): Promise<Vegetable> {
        const query = 'SELECT * FROM Vegetables WHERE id = ' + id;
        const request = new mssql.Request(this.db);
        const result = await request.query(query);
        const data: Vegetable = result.recordset[0]
        return data;
    }

    async delete(id: number): Promise<void> {
        const ps = new mssql.PreparedStatement(this.db);
        try {
            ps.input('id', mssql.Int);
            await ps.prepare('DELETE FROM Vegetables WHERE id = @id');
            await ps.execute({ id: id });
        }
        catch (err) {
            console.log("SQL Err", err.stack);
        } finally {
            await ps.unprepare();
        }
    }

    async add(item: Vegetable): Promise<void> {
        console.log(item);
        const ps = new mssql.PreparedStatement(this.db);
        try {
            ps.input('name', mssql.VarChar(MAX));
            ps.input('price', mssql.Decimal(8, 2));
            ps.input('amount', mssql.Int);
            await ps.prepare('INSERT INTO Vegetables(name, price, amount) VALUES(@name, @price, @amount)');
            await ps.execute({ name: item.name, price: item.price, amount: item.amount })
        }
        catch (err) {
            console.log("SQL Err", err.stack);
        } finally {
            await ps.unprepare();
        }
    }

    async update(item:Vegetable): Promise<void> {
        const ps = new mssql.PreparedStatement(this.db);
        try {
            ps.input('id', mssql.Int);
            ps.input('name', mssql.VarChar(MAX));
            ps.input('price', mssql.Decimal(8, 2));
            ps.input('amount', mssql.Int);
            await ps.prepare('UPDATE Vegetables SET name = @name, price = @price, amount = @amount WHERE id = @id');
            await ps.execute({ id: item.id, name: item.name, price: item.price, amount: item.amount })
        }
        catch (err) {
            console.log("SQL Err", err.stack);
        } finally {
            await ps.unprepare();
        }
    }
}
