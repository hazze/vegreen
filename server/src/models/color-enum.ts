export enum ColorEnum {
    Green = 1,
    Red,
    Orange,
    Yellow,
    Blue,
    Purple,
    Brown
}