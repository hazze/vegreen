import { Request, Response } from 'express';
import DatabaseHandler from '../db/database-handler';

export class VeggiesController {
    private db = new DatabaseHandler();

    constructor() {
        this.db.init();
    }

    public getAll = async (req: Request, res: Response) => {
        const data = await this.db.getAll();
        res.send(JSON.stringify(data));
    }

    public getById = async (req: Request, res: Response) => {
        const data = await this.db.getById(parseInt(req.params.id));
        res.send(JSON.stringify(data));
    }

    public update = async (req: Request, res: Response) => {
        try {
            this.db.update(req.body)
            res.sendStatus(200);
        }
        catch {
            res.sendStatus(500);
        }
    }

    public delete = (req: Request, res: Response) => {
        try {
            this.db.delete(parseInt(req.params.id))
            res.sendStatus(200);
        }
        catch {
            res.sendStatus(500);
        }
    }
    
    public add = (req: Request, res: Response) => {
        try {
            this.db.add(req.body);
            res.sendStatus(200);
        }
        catch {
            res.sendStatus(500);
        }
    }
}
