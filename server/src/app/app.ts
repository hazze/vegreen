import express, { Application, Request, Response, NextFunction } from 'express';
import { Routes } from '../routes/routes';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

dotenv.config();
class App {
    public app: Application;
    public routes: Routes = new Routes();

    constructor() {
        this.app = express();
        this.config();
        this.routes.routes(this.app);
    }

    private config() {
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,POST,DELETE,OPTIONS,PUT,PATCH');
            res.header('Access-Control-Allow-Headers', '*');
            next();
        });
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
    }
}
export default new App().app;
