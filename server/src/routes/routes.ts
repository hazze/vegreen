import { Application, Request, Response } from 'express';
import { VeggiesController } from '../controllers/veggies';

export class Routes {
    private veggies: VeggiesController = new VeggiesController();

    public routes(app: Application) {
        app.route('/api/veggies/')
            .get(this.veggies.getAll)
            .patch(this.veggies.update)
            .post(this.veggies.add);

        app.route('/api/veggies/:id')
            .get(this.veggies.getById)
            .delete(this.veggies.delete);
    }
}
